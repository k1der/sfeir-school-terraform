# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/google" {
  version = "4.21.0"
  hashes = [
    "h1:0YU2B3P4WM8rSPpulqZyPN0XfNLtp0mOEnonWHlRW5I=",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version = "3.2.0"
  hashes = [
    "h1:eeUh6cJ6wKLLuo4q9uQ0CA1Zvfqya4Wn1LecLCN8KKs=",
  ]
}

provider "registry.terraform.io/hashicorp/vault" {
  version = "3.6.0"
  hashes = [
    "h1:bXAbuGy5MStp9n4dqmeueEutLHuuWpYWZBFb6YrPWu8=",
  ]
}
