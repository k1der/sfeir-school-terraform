resource "random_password" "password" {
  // Create a new password with special chars and 16 characters
  length  = 16
  special = true
}

resource "google_sql_database_instance" "master" {
  // Create a new sql database with variables.tf content
  name = var.instance_name
  database_version = var.database_version
  region = var.region

  // We allow internet access only for lab purpose
  settings {
    tier = "db-f1-micro"
    ip_configuration {
      ipv4_enabled = true
      authorized_networks {
        name  = "internet"
        value = "0.0.0.0/0"
      }
    }
  }
}

resource "google_sql_user" "users" {
  name     = "me@example.com"
  instance = google_sql_database_instance.master.name
  password = random_password.password.result
}

resource "vault_generic_secret" "example" {
  // Put the password in vault
  path = "secret/tbp-demo-user"

  data_json = <<EOT
{
  "user":   "${google_sql_user.users.name}",
  "password": "${random_password.password.result}"
}
EOT
}

provider "vault" {
  address = "http://34.79.109.130:8200/"
  token = "s.9i8xqnGnXLuioSoNOsa4hIyG"
}

